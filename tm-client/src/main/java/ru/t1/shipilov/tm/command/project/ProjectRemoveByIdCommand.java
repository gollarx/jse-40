package ru.t1.shipilov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-remove-by-id";

    @NotNull
    private final String DESCRIPTION = "Remove project by Id.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().removeProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
