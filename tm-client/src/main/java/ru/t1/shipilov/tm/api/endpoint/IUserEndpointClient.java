package ru.t1.shipilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.request.*;
import ru.t1.shipilov.tm.dto.response.*;

public interface IUserEndpointClient extends IEndpointClient {

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserProfileResponse showUserProfile(@NotNull final UserProfileRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

}
