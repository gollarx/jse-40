package ru.t1.shipilov.tm.api.service;

import org.apache.ibatis.session.SqlSession;

public interface IConnectionService {

    SqlSession getSqlSession();

}
